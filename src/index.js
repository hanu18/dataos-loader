import React from 'react'
import styles from './styles.module.css'

export const Loader = ({ text="Loading...", style={}, className="" }) => {
  return <div className={`display-4 card card-body border-0 ${className}`} style={{...{ opacity: '0.1' }, ...style}}> {text} </div>
}
