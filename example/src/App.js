import React from 'react'

import { Loader } from 'dataos-loader'
import 'dataos-loader/dist/index.css'

const App = () => {
  return <Loader text="Loading..." style={{ opacity: '0.5' }} className="text-danger"/>
}

export default App
