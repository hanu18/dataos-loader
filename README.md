# dataos-loader

> Common loader for data os

[![NPM](https://img.shields.io/npm/v/dataos-loader.svg)](https://www.npmjs.com/package/dataos-loader) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save dataos-loader
```

## Usage

```jsx
import React, { Component } from 'react'

import { Loader } from 'dataos-loader'
import 'dataos-loader/dist/index.css'

class Example extends Component {
  render() {
    return <Loader text="Loading..." style={{ opacity: '0.5' }} className="text-danger"/>
  }
}
```

## License

MIT © [hanu18](https://github.com/hanu18)
